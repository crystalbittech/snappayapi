var express = require('express');
var mongoose = require('mongoose');
var user = express.Router()
var userModel =mongoose.model('UserModel');
// middleware that is specific to this router
user.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
});

user.get('/inquire', function (req, res) {
    res.send('Create Account')
});


module.exports = user