var express = require('express')
var account = express.Router()

// middleware that is specific to this router
account.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

account.get('/', function (req, res) {
  res.send('Welcome to Accounts Home Page')
})

account.post('/create', function (req, res) {
  
  res.send('Create Account')
})

account.get('/inquire', function (req, res) {
    res.send('Create Account')
})


module.exports = account