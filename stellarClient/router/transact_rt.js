var express = require('express')
var router = express.Router()

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
  console.log('Transact API called Time: ', Date.now())//sequence or priviledges can be validated here
  next()
})
// define the home page route
router.get('/', function (req, res) {
  res.send('Welcome to Accounts Home Page')
})
// define the about route
router.post('/initiate', function (req, res) {
  res.send('Create Account')
})

router.get('/inquire/:token_id', function (req, res) {
    res.send('Create Account')
})

router.get('/reverse', function (req, res) {
    res.send('Create Account')
})

router.get('/validate', function (req, res) {
    res.send('Create Account')
})

module.exports = router