var express     = require('express');
var app         = express();
var mongoose    = require('mongoose');
var logger      = require('morgan');
var bodyParser  = require('body-parser');
var cors        = require('cors');
var StellarSdk = require('stellar-sdk');
const url = 'mongodb://127.0.0.1:27017/markers';
const ur2 = 'mongodb://admin:admin123@ds151820.mlab.com:51820/crystaldb';
app.use(bodyParser.urlencoded({ extended: false })); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses
app.use(logger('dev')); // Log requests to API using morgan
app.use(cors());
var router = express.Router();  
var server = new StellarSdk.Server('https://horizon-testnet.stellar.org');


var db;
var User; var Transaction; var Account;
mongoose.Promise = global.Promise;
mongoose.connect(ur2,function (err, database){
	if (err) {
		console.log(err);
		console.log("Database connection failed "+err);
		process.exit(1);
	}
	db = database;
	console.log("Database connection ready"); 
	var server = app.listen(process.env.PORT || 8087, function () {
		var port = server.address().port;
		console.log("App now running on port", port);
	});
	Account     		= require('../stellarClient/models/account_md');
	Transaction     	= require('../stellarClient/models/transact_md');
	User     			= require('../stellarClient/models/user_md');
	 
});

router.route('/createKey')
.post(function(req, res) {
	var acctDetails = {};
	acctDetails = req.body;
	console.log("querying with data: "+JSON.stringify(acctDetails));
	User.find({$or:[{email:acctDetails.email},{phoneNumber:acctDetails.phonenumber}]},function(err, users) {
		if (err)
			res.json(err);
		console.log("Found "+users);
		if(users!=""&&users.accountID!=""){
			res.json({ message: 'Account Already Exists',data:{"publicKey":users[0].publicKey}});
		}else{
			
			var pairer = createSeedandpubKey();			
			var pubKey = pairer.publicKey();
			var seed   = pairer.secret();
				console.log('SUCCESS! You have a new account :)\n', body);
				user = new User();
				user.uid 			= acctDetails.id;
				user.username 		= acctDetails.username;
				user.passkey  		= acctDetails.passkey;
				user.email 			= acctDetails.email;
				user.phoneNumber  	= acctDetails.phonenumber;
				user.seed 			= seed;
				user.publicKey 		= pubKey;
				user.save(function(err) {
					if (err)
						res.send(err);
					console.log("user created wihh data "+user);
					res.json({ message: 'user created!',data:user});
				});

		}
	});	
})



router.route('/account/:key')
.post(function(req, res) {
		var pubkey = req.params.key;
		
		console.log("querying with data: "+pubkey);
		User.find({publicKey:pubkey},function(err, users) {
			if (err)
				res.json(err);
			// console.log("Found Key for: "+users);
			if(users!=""&&users.accountID!=""){
				var request = require('request');
				request.get({
					url: 'https://horizon-testnet.stellar.org/friendbot',
					qs: { addr: pubkey },
					json: true
				}, function(error, response, body) {
				if (error || response.statusCode !== 200) {
					console.error('ERROR!', error || body);
				}
				else {
					console.log('SUCCESS! You have a new account :)\n', body);
					server.loadAccount(pubKey).then(function(account) {
						console.log('Balances for account: ' + pubKey);
						console.log(JSON.stringify(account))
						account.balances.forEach(function(balance) {
							console.log('Type:', balance.asset_type, ', Balance:', balance.balance);
						});
					});

				}});
			}else{
				res.json({ message: 'Invalid key Supplied'});			
			}
		});
		
})
.get(function(req,res){
	var pubKey = req.params.key;
	var server = new StellarSdk.Server('https://horizon-testnet.stellar.org');

	// the JS SDK uses promises for most actions, such as retrieving an account
	server.loadAccount(pubKey).then(function(account) {
		console.log('Balances for account: ' + pubKey);
		account.balances.forEach(function(balance) {
			console.log('Type:', balance.asset_type, ', Balance:', balance.balance);
		});
	});
	res.json({ message: 'Account Supplied for: '+pubKey});		
});

router.route('/fetchAccounts')
.get(function(req, res) {
        User.find(function(err, users) {
            if (err)
                res.send(err);

            res.json(users);
        });
});


router.route('/transact')
.post(function(req, res) {
	var tranDetails = {};
	tranDetails = req.body;
	console.log("querying with data: "+JSON.stringify(tranDetails));
	Transaction.find({tranrequestid:tranDetails.tranreqid},function(err, trans) {
		if (err)
			res.json(err);
		console.log("Found "+trans);
		if(trans!=""&&trans.tranrequestid!=""){
			res.json({ message: 'Transaction Already Exists',data:{"publicKey":trans[0].tranStatus}});
		}else{

			Transact();
			res.json({ message: 'Tran created!'});
			// var pairer = createSeedandpubKey();			
			// var pubKey = pairer.publicKey();
			// var seed   = pairer.secret();
			// 	console.log('SUCCESS! You have a new account :)\n', body);
			// 	user = new User();
			// 	user.uid 			= tranDetails.id;
			// 	user.username 		= acctDetails.username;
			// 	user.passkey  		= acctDetails.passkey;
			// 	user.email 			= acctDetails.email;
			// 	user.phoneNumber  	= acctDetails.phonenumber;
			// 	user.seed 			= seed;
			// 	user.publicKey 		= pubKey;
			// 	user.save(function(err) {
			// 		if (err)
			// 			res.send(err);
			// 		console.log("user created wihh data "+user);
			// 		res.json({ message: 'user created!',data:user});
			// 	});

		}
	});	
})

function createSeedandpubKey(){
	var pair = StellarSdk.Keypair.random();
	var pubKey = pair.publicKey();
	var seed = pair.secret();
	console.log("Created Public Key: "+pubKey+" and Seed "+seed);
	return pair;
	
}

function Transact(){
	var StellarSdk = require('stellar-sdk');
	StellarSdk.Network.useTestNetwork();
	var server = new StellarSdk.Server('https://horizon-testnet.stellar.org');
	var sourceKeys = StellarSdk.Keypair
	.fromSecret('SCZANGBA5YHTNYVVV4C3U252E2B6P6F5T3U6MM63WBSBZATAQI3EBTQ4');
	var destinationId = 'GA2C5RFPE6GCKMY3US5PAB6UZLKIGSPIUKSLRB6Q723BM2OARMDUYEJ5';
	// Transaction will hold a built transaction we can resubmit if the result is unknown.
	var transaction;

	// First, check to make sure that the destination account exists.
	// You could skip this, but if the account does not exist, you will be charged
	// the transaction fee when the transaction fails.
	server.loadAccount(destinationId)
	// If the account is not found, surface a nicer error message for logging.
	.catch(StellarSdk.NotFoundError, function (error) {
		throw new Error('The destination account does not exist!');
	})
	// If there was no error, load up-to-date information on your account.
	.then(function() {
		return server.loadAccount(sourceKeys.publicKey());
	})
	.then(function(sourceAccount) {
		// Start building the transaction.
		transaction = new StellarSdk.TransactionBuilder(sourceAccount)
		.addOperation(StellarSdk.Operation.payment({
			destination: destinationId,
			// Because Stellar allows transaction in many currencies, you must
			// specify the asset type. The special "native" asset represents Lumens.
			asset: StellarSdk.Asset.native(),
			amount: "10"
		}))
		// A memo allows you to add your own metadata to a transaction. It's
		// optional and does not affect how Stellar treats the transaction.
		.addMemo(StellarSdk.Memo.text('Test Transaction'))
		.build();
		// Sign the transaction to prove you are actually the person sending it.
		transaction.sign(sourceKeys);
		// And finally, send it off to Stellar!
		return server.submitTransaction(transaction);
	})
	.then(function(result) {
		console.log('Success! Results:', result);
	})
	.catch(function(error) {
		console.error('Something went wrong!', error);
		// If the result is unknown (no response body, timeout etc.) we simply resubmit
		// already built transaction:
		// server.submitTransaction(transaction);
	});
}


router.route('/')
.get(function(req,res){
	res.send('Welcome to Home Page');
});

app.use('/api', router);
// app.listen(8087);
// console.log("App listening on port 8087");