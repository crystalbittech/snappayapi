var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var AccountSchema   = new Schema({
  id: Number,
  accountID: String,
  accountName: String,
  balance:[{assetType:String,assetValue:Number}],
  accountOpnDate: String,
  lastTranID: String,
  lastTranDate:Number
});

module.exports = mongoose.model('AccountModel', AccountSchema);