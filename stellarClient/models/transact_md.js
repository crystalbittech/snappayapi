var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TransationSchema   = new Schema({
  tranrequestid: String,
  tranperiod:Number,
  tranmethod:String,
  transactionid: String,
  tranRate:Number,
  tipamount:Number,
  tranAmt: Number,
  tranStatus: String,
  trandescription: String,
  trandate: Number,
  trancredperson:String,
  trandebitperson: String
});

module.exports = mongoose.model('Transaction', TransationSchema);