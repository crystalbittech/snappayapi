var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
  uid: Number,
  username: String,
  email: String,
  passkey: String,
  phoneNumber:String,
  seed:String,
  publicKey:String,
  accountID:String
});

module.exports = mongoose.model('User', UserSchema);